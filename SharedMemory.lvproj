﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="11008008">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This project is used to maintain the SharedMemory interface.

1) Memory that can be shared between different Windows applications (shared memory).
2) Global memory for applications (not shared between different Windows applications)

The functionality is provided by a dll whic can be distributed under the terms of the GPL. 

</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="doc" Type="Folder">
			<Item Name="SharedMem-VI-Tree.html" Type="Document" URL="../doc/SharedMem-VI-Tree.html"/>
			<Item Name="SharedMem-VI-Treec.png" Type="Document" URL="../doc/SharedMem-VI-Treec.png"/>
			<Item Name="SharedMem-VI-Treed.png" Type="Document" URL="../doc/SharedMem-VI-Treed.png"/>
			<Item Name="SharedMem-VI-Treep.png" Type="Document" URL="../doc/SharedMem-VI-Treep.png"/>
			<Item Name="SharedMem_ByteArray2Datac.png" Type="Document" URL="../doc/SharedMem_ByteArray2Datac.png"/>
			<Item Name="SharedMem_CreateBufferc.png" Type="Document" URL="../doc/SharedMem_CreateBufferc.png"/>
			<Item Name="SharedMem_CreateBufferd.png" Type="Document" URL="../doc/SharedMem_CreateBufferd.png"/>
			<Item Name="SharedMem_CreateBufferd1.png" Type="Document" URL="../doc/SharedMem_CreateBufferd1.png"/>
			<Item Name="SharedMem_CreateBufferd2.png" Type="Document" URL="../doc/SharedMem_CreateBufferd2.png"/>
			<Item Name="SharedMem_CreateBufferp.png" Type="Document" URL="../doc/SharedMem_CreateBufferp.png"/>
			<Item Name="SharedMem_Data2ByteArrayc.png" Type="Document" URL="../doc/SharedMem_Data2ByteArrayc.png"/>
			<Item Name="SharedMem_DestroyBufferc.png" Type="Document" URL="../doc/SharedMem_DestroyBufferc.png"/>
			<Item Name="SharedMem_DestroyBufferd.png" Type="Document" URL="../doc/SharedMem_DestroyBufferd.png"/>
			<Item Name="SharedMem_DestroyBufferd1.png" Type="Document" URL="../doc/SharedMem_DestroyBufferd1.png"/>
			<Item Name="SharedMem_DestroyBufferp.png" Type="Document" URL="../doc/SharedMem_DestroyBufferp.png"/>
			<Item Name="SharedMem_error_messagec.png" Type="Document" URL="../doc/SharedMem_error_messagec.png"/>
			<Item Name="SharedMem_error_messaged.png" Type="Document" URL="../doc/SharedMem_error_messaged.png"/>
			<Item Name="SharedMem_error_messaged1.png" Type="Document" URL="../doc/SharedMem_error_messaged1.png"/>
			<Item Name="SharedMem_error_messaged2.png" Type="Document" URL="../doc/SharedMem_error_messaged2.png"/>
			<Item Name="SharedMem_error_messagep.png" Type="Document" URL="../doc/SharedMem_error_messagep.png"/>
			<Item Name="SharedMem_GetBufferc.png" Type="Document" URL="../doc/SharedMem_GetBufferc.png"/>
			<Item Name="SharedMem_GetBufferd.png" Type="Document" URL="../doc/SharedMem_GetBufferd.png"/>
			<Item Name="SharedMem_GetBufferd1.png" Type="Document" URL="../doc/SharedMem_GetBufferd1.png"/>
			<Item Name="SharedMem_GetBufferd2.png" Type="Document" URL="../doc/SharedMem_GetBufferd2.png"/>
			<Item Name="SharedMem_GetBufferIDc.png" Type="Document" URL="../doc/SharedMem_GetBufferIDc.png"/>
			<Item Name="SharedMem_GetBufferIDd.png" Type="Document" URL="../doc/SharedMem_GetBufferIDd.png"/>
			<Item Name="SharedMem_GetBufferIDd1.png" Type="Document" URL="../doc/SharedMem_GetBufferIDd1.png"/>
			<Item Name="SharedMem_GetBufferIDd2.png" Type="Document" URL="../doc/SharedMem_GetBufferIDd2.png"/>
			<Item Name="SharedMem_GetBufferIDp.png" Type="Document" URL="../doc/SharedMem_GetBufferIDp.png"/>
			<Item Name="SharedMem_GetBufferNamec.png" Type="Document" URL="../doc/SharedMem_GetBufferNamec.png"/>
			<Item Name="SharedMem_GetBufferNamed.png" Type="Document" URL="../doc/SharedMem_GetBufferNamed.png"/>
			<Item Name="SharedMem_GetBufferNamed1.png" Type="Document" URL="../doc/SharedMem_GetBufferNamed1.png"/>
			<Item Name="SharedMem_GetBufferNamed2.png" Type="Document" URL="../doc/SharedMem_GetBufferNamed2.png"/>
			<Item Name="SharedMem_GetBufferNamep.png" Type="Document" URL="../doc/SharedMem_GetBufferNamep.png"/>
			<Item Name="SharedMem_GetBufferp.png" Type="Document" URL="../doc/SharedMem_GetBufferp.png"/>
			<Item Name="SharedMem_GetBufferPointerc.png" Type="Document" URL="../doc/SharedMem_GetBufferPointerc.png"/>
			<Item Name="SharedMem_GetBufferPointerd.png" Type="Document" URL="../doc/SharedMem_GetBufferPointerd.png"/>
			<Item Name="SharedMem_GetBufferPointerd1.png" Type="Document" URL="../doc/SharedMem_GetBufferPointerd1.png"/>
			<Item Name="SharedMem_GetBufferPointerd2.png" Type="Document" URL="../doc/SharedMem_GetBufferPointerd2.png"/>
			<Item Name="SharedMem_GetBufferPointerp.png" Type="Document" URL="../doc/SharedMem_GetBufferPointerp.png"/>
			<Item Name="SharedMem_GetBufferSizec.png" Type="Document" URL="../doc/SharedMem_GetBufferSizec.png"/>
			<Item Name="SharedMem_GetBufferSized.png" Type="Document" URL="../doc/SharedMem_GetBufferSized.png"/>
			<Item Name="SharedMem_GetBufferSized1.png" Type="Document" URL="../doc/SharedMem_GetBufferSized1.png"/>
			<Item Name="SharedMem_GetBufferSized2.png" Type="Document" URL="../doc/SharedMem_GetBufferSized2.png"/>
			<Item Name="SharedMem_GetBufferSizep.png" Type="Document" URL="../doc/SharedMem_GetBufferSizep.png"/>
			<Item Name="SharedMem_GetProcCounterc.png" Type="Document" URL="../doc/SharedMem_GetProcCounterc.png"/>
			<Item Name="SharedMem_GetProcCounterd.png" Type="Document" URL="../doc/SharedMem_GetProcCounterd.png"/>
			<Item Name="SharedMem_GetProcCounterd1.png" Type="Document" URL="../doc/SharedMem_GetProcCounterd1.png"/>
			<Item Name="SharedMem_GetProcCounterp.png" Type="Document" URL="../doc/SharedMem_GetProcCounterp.png"/>
			<Item Name="SharedMem_GetSharedBufferc.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferc.png"/>
			<Item Name="SharedMem_GetSharedBufferd.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferd.png"/>
			<Item Name="SharedMem_GetSharedBufferd1.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferd1.png"/>
			<Item Name="SharedMem_GetSharedBufferd2.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferd2.png"/>
			<Item Name="SharedMem_GetSharedBufferp.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferp.png"/>
			<Item Name="SharedMem_GetSharedBufferPointerc.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferPointerc.png"/>
			<Item Name="SharedMem_GetSharedBufferPointerd.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferPointerd.png"/>
			<Item Name="SharedMem_GetSharedBufferPointerd1.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferPointerd1.png"/>
			<Item Name="SharedMem_GetSharedBufferPointerd2.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferPointerd2.png"/>
			<Item Name="SharedMem_GetSharedBufferPointerp.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferPointerp.png"/>
			<Item Name="SharedMem_GetSharedBufferSizec.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferSizec.png"/>
			<Item Name="SharedMem_GetSharedBufferSized.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferSized.png"/>
			<Item Name="SharedMem_GetSharedBufferSized1.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferSized1.png"/>
			<Item Name="SharedMem_GetSharedBufferSized2.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferSized2.png"/>
			<Item Name="SharedMem_GetSharedBufferSizep.png" Type="Document" URL="../doc/SharedMem_GetSharedBufferSizep.png"/>
			<Item Name="SharedMem_GlobalMemApplicationExamplec.png" Type="Document" URL="../doc/SharedMem_GlobalMemApplicationExamplec.png"/>
			<Item Name="SharedMem_GlobalMemApplicationExampled.png" Type="Document" URL="../doc/SharedMem_GlobalMemApplicationExampled.png"/>
			<Item Name="SharedMem_GlobalMemApplicationExampled1.png" Type="Document" URL="../doc/SharedMem_GlobalMemApplicationExampled1.png"/>
			<Item Name="SharedMem_GlobalMemApplicationExamplep.png" Type="Document" URL="../doc/SharedMem_GlobalMemApplicationExamplep.png"/>
			<Item Name="SharedMem_GlobalMemSpeedTestc.png" Type="Document" URL="../doc/SharedMem_GlobalMemSpeedTestc.png"/>
			<Item Name="SharedMem_GlobalMemSpeedTestd.png" Type="Document" URL="../doc/SharedMem_GlobalMemSpeedTestd.png"/>
			<Item Name="SharedMem_GlobalMemSpeedTestp.png" Type="Document" URL="../doc/SharedMem_GlobalMemSpeedTestp.png"/>
			<Item Name="SharedMem_InitBufferc.png" Type="Document" URL="../doc/SharedMem_InitBufferc.png"/>
			<Item Name="SharedMem_InitBufferd.png" Type="Document" URL="../doc/SharedMem_InitBufferd.png"/>
			<Item Name="SharedMem_InitBufferd1.png" Type="Document" URL="../doc/SharedMem_InitBufferd1.png"/>
			<Item Name="SharedMem_InitBufferd2.png" Type="Document" URL="../doc/SharedMem_InitBufferd2.png"/>
			<Item Name="SharedMem_InitBufferp.png" Type="Document" URL="../doc/SharedMem_InitBufferp.png"/>
			<Item Name="SharedMem_SetBufferc.png" Type="Document" URL="../doc/SharedMem_SetBufferc.png"/>
			<Item Name="SharedMem_SetBufferd.png" Type="Document" URL="../doc/SharedMem_SetBufferd.png"/>
			<Item Name="SharedMem_SetBufferd1.png" Type="Document" URL="../doc/SharedMem_SetBufferd1.png"/>
			<Item Name="SharedMem_SetBufferd2.png" Type="Document" URL="../doc/SharedMem_SetBufferd2.png"/>
			<Item Name="SharedMem_SetBufferd3.png" Type="Document" URL="../doc/SharedMem_SetBufferd3.png"/>
			<Item Name="SharedMem_SetBufferp.png" Type="Document" URL="../doc/SharedMem_SetBufferp.png"/>
			<Item Name="SharedMem_SetSharedBufferc.png" Type="Document" URL="../doc/SharedMem_SetSharedBufferc.png"/>
			<Item Name="SharedMem_SetSharedBufferd.png" Type="Document" URL="../doc/SharedMem_SetSharedBufferd.png"/>
			<Item Name="SharedMem_SetSharedBufferd1.png" Type="Document" URL="../doc/SharedMem_SetSharedBufferd1.png"/>
			<Item Name="SharedMem_SetSharedBufferd2.png" Type="Document" URL="../doc/SharedMem_SetSharedBufferd2.png"/>
			<Item Name="SharedMem_SetSharedBufferp.png" Type="Document" URL="../doc/SharedMem_SetSharedBufferp.png"/>
			<Item Name="SharedMem_SharedMemApplicationExamplec.png" Type="Document" URL="../doc/SharedMem_SharedMemApplicationExamplec.png"/>
			<Item Name="SharedMem_SharedMemApplicationExampled.png" Type="Document" URL="../doc/SharedMem_SharedMemApplicationExampled.png"/>
			<Item Name="SharedMem_SharedMemApplicationExamplep.png" Type="Document" URL="../doc/SharedMem_SharedMemApplicationExamplep.png"/>
			<Item Name="SharedMem_SharedMemSpeedTestc.png" Type="Document" URL="../doc/SharedMem_SharedMemSpeedTestc.png"/>
			<Item Name="SharedMem_SharedMemSpeedTestd.png" Type="Document" URL="../doc/SharedMem_SharedMemSpeedTestd.png"/>
			<Item Name="SharedMem_SharedMemSpeedTestp.png" Type="Document" URL="../doc/SharedMem_SharedMemSpeedTestp.png"/>
		</Item>
		<Item Name="SharedMem.lvlib" Type="Library" URL="../SharedMem.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="SharedMemoryMaster" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{D942B449-040E-4F26-9F07-512ADDF54BC9}</Property>
				<Property Name="App_INI_GUID" Type="Str">{21B191C9-73E9-4E63-95AA-869FCED3910B}</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{9AC7422D-57C1-4660-B2FC-2A523C73405C}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">SharedMemoryMaster</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{25B38132-5316-42E1-8926-3EC457C745E4}</Property>
				<Property Name="Destination[0].destName" Type="Str">SharedMemoryMaster.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/SharedMemoryMaster.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{15E24323-C5DD-422D-A3CE-700712A1D02E}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/SharedMem.lvlib/public/Examples/SharedMem_Master.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/SharedMem.lvlib/SharedMemVisualC/SharedMem.dll</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Darmstadt</Property>
				<Property Name="TgtF_fileDescription" Type="Str">SharedMemoryMaster</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">SharedMemoryMaster</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2011 GSI Darmstadt</Property>
				<Property Name="TgtF_productName" Type="Str">SharedMemoryMaster</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{93119A75-A7E9-4C00-B5A3-975B0028A2F6}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">SharedMemoryMaster.exe</Property>
			</Item>
			<Item Name="SharedMemorySlave" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{31D207A0-3574-4A5C-A003-5856A3F4EC90}</Property>
				<Property Name="App_INI_GUID" Type="Str">{4E2888B8-1153-4261-B671-53377C0E022A}</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{D944CC25-D1D0-41D8-93B3-AC5A5E8BF151}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">SharedMemorySlave</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{9740DEB0-E316-41FC-BF6A-BE9531D831FF}</Property>
				<Property Name="Destination[0].destName" Type="Str">SharedMemorySlave.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/SharedMemorySlave.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{15E24323-C5DD-422D-A3CE-700712A1D02E}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/SharedMem.lvlib/public/Examples/SharedMem_Master.vi</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/SharedMem.lvlib/SharedMemVisualC/SharedMem.dll</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/SharedMem.lvlib/public/Examples/SharedMem_Slave.vi</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">4</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Darmstadt</Property>
				<Property Name="TgtF_fileDescription" Type="Str">SharedMemoryMaster</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">SharedMemoryMaster</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2011 GSI Darmstadt</Property>
				<Property Name="TgtF_productName" Type="Str">SharedMemoryMaster</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{F5F1A079-BFA8-42C4-9610-5089B2705069}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">SharedMemorySlave.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
