﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="11008008">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_2011_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 2011\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_2011_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 2011\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is a tool that provides two things.

1) Memory that can be shared between different Windows applications (shared memory).
2) Global memory for applications (not shared between different Windows applications)

The functionality is provided by a dll whic can be distributed under the terms of the GPL. This instrument driver is a wrapper around that dll.

Attached is a small program test2.exe. It writes the numbers 0..99 to the first 100 bytes of the shared memory. Then it writes the number 17 to the first 10 bytes. Then, every 100ms, it writes a loop index to the first 10bytes.

History: Version 0.0, 8th of April 2004
1st Nov 2011 H.Brand@gsi.de Converted to LV 2011 lvlib and added to Example-VIs with corresponding Build-Specifications.

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation; either version 2 of the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: D. Beck, M.Richter@gsi.de or H.Brand@gsi.de.
Last update: 07-APR-2004</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.2011</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="private" Type="Folder">
		<Item Name="SharedMem_ByteArray2DblArrayData.vi" Type="VI" URL="../SharedMemxxu/SharedMem_ByteArray2DblArrayData.vi"/>
		<Item Name="SharedMem_ByteArray2DoubleData.vi" Type="VI" URL="../SharedMemxxu/SharedMem_ByteArray2DoubleData.vi"/>
		<Item Name="SharedMem_ByteArray2FloatData.vi" Type="VI" URL="../SharedMemxxu/SharedMem_ByteArray2FloatData.vi"/>
		<Item Name="SharedMem_ByteArray2IntArrayData.vi" Type="VI" URL="../SharedMemxxu/SharedMem_ByteArray2IntArrayData.vi"/>
		<Item Name="SharedMem_ByteArray2IntegerData.vi" Type="VI" URL="../SharedMemxxu/SharedMem_ByteArray2IntegerData.vi"/>
		<Item Name="SharedMem_ByteArray2SglArrayData.vi" Type="VI" URL="../SharedMemxxu/SharedMem_ByteArray2SglArrayData.vi"/>
		<Item Name="SharedMem_ByteArray2ShortArrayData.vi" Type="VI" URL="../SharedMemxxu/SharedMem_ByteArray2ShortArrayData.vi"/>
		<Item Name="SharedMem_ByteArray2ShortData.vi" Type="VI" URL="../SharedMemxxu/SharedMem_ByteArray2ShortData.vi"/>
		<Item Name="SharedMem_ByteArray2SingleData.vi" Type="VI" URL="../SharedMemxxu/SharedMem_ByteArray2SingleData.vi"/>
		<Item Name="SharedMem_ByteArray2StringData.vi" Type="VI" URL="../SharedMemxxu/SharedMem_ByteArray2StringData.vi"/>
		<Item Name="SharedMem_ByteArray2UnsignedData.vi" Type="VI" URL="../SharedMemxxu/SharedMem_ByteArray2UnsignedData.vi"/>
		<Item Name="SharedMem_DataDblArray2ByteArray.vi" Type="VI" URL="../SharedMemxxu/SharedMem_DataDblArray2ByteArray.vi"/>
		<Item Name="SharedMem_DataDouble2ByteArray.vi" Type="VI" URL="../SharedMemxxu/SharedMem_DataDouble2ByteArray.vi"/>
		<Item Name="SharedMem_DataIntArray2ByteArray.vi" Type="VI" URL="../SharedMemxxu/SharedMem_DataIntArray2ByteArray.vi"/>
		<Item Name="SharedMem_DataInteger2ByteArray.vi" Type="VI" URL="../SharedMemxxu/SharedMem_DataInteger2ByteArray.vi"/>
		<Item Name="SharedMem_DataSglArray2ByteArray.vi" Type="VI" URL="../SharedMemxxu/SharedMem_DataSglArray2ByteArray.vi"/>
		<Item Name="SharedMem_DataShort2ByteArray.vi" Type="VI" URL="../SharedMemxxu/SharedMem_DataShort2ByteArray.vi"/>
		<Item Name="SharedMem_DataShortArray2ByteArray.vi" Type="VI" URL="../SharedMemxxu/SharedMem_DataShortArray2ByteArray.vi"/>
		<Item Name="SharedMem_DataSingle2ByteArray.vi" Type="VI" URL="../SharedMemxxu/SharedMem_DataSingle2ByteArray.vi"/>
		<Item Name="SharedMem_DataString2ByteArray.vi" Type="VI" URL="../SharedMemxxu/SharedMem_DataString2ByteArray.vi"/>
		<Item Name="SharedMem_DataUnsigned2ByteArray.vi" Type="VI" URL="../SharedMemxxu/SharedMem_DataUnsigned2ByteArray.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="Examples" Type="Folder">
			<Item Name="SharedMem-SpeedTest.vi" Type="VI" URL="../SharedMemxxx/SharedMem-SpeedTest.vi"/>
			<Item Name="SharedMem_GlobalMemApplicationExample.vi" Type="VI" URL="../SharedMemxxx/SharedMem_GlobalMemApplicationExample.vi"/>
			<Item Name="SharedMem_GlobalMemSpeedTest.vi" Type="VI" URL="../SharedMemxxx/SharedMem_GlobalMemSpeedTest.vi"/>
			<Item Name="SharedMem_Master.vi" Type="VI" URL="../SharedMemxxx/SharedMem_Master.vi"/>
			<Item Name="SharedMem_SharedMemApplicationExample.vi" Type="VI" URL="../SharedMemxxx/SharedMem_SharedMemApplicationExample.vi"/>
			<Item Name="SharedMem_SharedMemSpeedTest.vi" Type="VI" URL="../SharedMemxxx/SharedMem_SharedMemSpeedTest.vi"/>
			<Item Name="SharedMem_Slave.vi" Type="VI" URL="../SharedMemxxx/SharedMem_Slave.vi"/>
		</Item>
		<Item Name="Global Memory" Type="Folder">
			<Item Name="Data" Type="Folder">
				<Item Name="SharedMem_CreateBuffer.vi" Type="VI" URL="../SharedMemxxx/SharedMem_CreateBuffer.vi"/>
				<Item Name="SharedMem_DestroyBuffer.vi" Type="VI" URL="../SharedMemxxx/SharedMem_DestroyBuffer.vi"/>
				<Item Name="SharedMem_GetBuffer.vi" Type="VI" URL="../SharedMemxxx/SharedMem_GetBuffer.vi"/>
				<Item Name="SharedMem_InitBuffer.vi" Type="VI" URL="../SharedMemxxx/SharedMem_InitBuffer.vi"/>
				<Item Name="SharedMem_SetBuffer.vi" Type="VI" URL="../SharedMemxxx/SharedMem_SetBuffer.vi"/>
			</Item>
			<Item Name="Utility" Type="Folder">
				<Item Name="SharedMem_GetBufferID.vi" Type="VI" URL="../SharedMemxxx/SharedMem_GetBufferID.vi"/>
				<Item Name="SharedMem_GetBufferName.vi" Type="VI" URL="../SharedMemxxx/SharedMem_GetBufferName.vi"/>
				<Item Name="SharedMem_GetBufferPointer.vi" Type="VI" URL="../SharedMemxxx/SharedMem_GetBufferPointer.vi"/>
				<Item Name="SharedMem_GetBufferSize.vi" Type="VI" URL="../SharedMemxxx/SharedMem_GetBufferSize.vi"/>
			</Item>
		</Item>
		<Item Name="Shared Memory" Type="Folder">
			<Item Name="Data" Type="Folder">
				<Item Name="SharedMem_GetSharedBuffer.vi" Type="VI" URL="../SharedMemxxx/SharedMem_GetSharedBuffer.vi"/>
				<Item Name="SharedMem_SetSharedBuffer.vi" Type="VI" URL="../SharedMemxxx/SharedMem_SetSharedBuffer.vi"/>
			</Item>
			<Item Name="Utility" Type="Folder">
				<Item Name="SharedMem_GetSharedBufferPointer.vi" Type="VI" URL="../SharedMemxxx/SharedMem_GetSharedBufferPointer.vi"/>
				<Item Name="SharedMem_GetSharedBufferSize.vi" Type="VI" URL="../SharedMemxxx/SharedMem_GetSharedBufferSize.vi"/>
			</Item>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="SharedMem_ByteArray2Data.vi" Type="VI" URL="../SharedMemxxx/SharedMem_ByteArray2Data.vi"/>
			<Item Name="SharedMem_Data2ByteArray.vi" Type="VI" URL="../SharedMemxxx/SharedMem_Data2ByteArray.vi"/>
			<Item Name="SharedMem_error_message.vi" Type="VI" URL="../SharedMemxxx/SharedMem_error_message.vi"/>
			<Item Name="SharedMem_GetProcCounter.vi" Type="VI" URL="../SharedMemxxx/SharedMem_GetProcCounter.vi"/>
		</Item>
		<Item Name="SharedMem-VI-Tree.vi" Type="VI" URL="../SharedMemxxx/SharedMem-VI-Tree.vi"/>
	</Item>
	<Item Name="SharedMemVisualC" Type="Folder">
		<Item Name="SharedMem.dll" Type="Document" URL="../SharedMemVisualC/SharedMem.dll"/>
		<Item Name="SharedMem.pdf" Type="Document" URL="../SharedMemVisualC/SharedMem.pdf"/>
	</Item>
	<Item Name="license.txt" Type="Document" URL="../license.txt"/>
</Library>
